import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.navigateToUrl(GlobalVariable.defaultURL)

//Delete items in shopping cart if found any
Boolean shoppingCartItemCountNotZero = !(WebUI.getText(findTestObject('GlobalObjects/basket_ItemCount')).equals(
    '0'))

'see if there is anything in the cart, delete if any'
if (shoppingCartItemCountNotZero) {
    'go to basket'
    WebUI.click(findTestObject('GlobalObjects/basket_Icon'))

    Boolean crossBtnFound = WebUI.verifyElementPresent(findTestObject('Basket/basket_DeleteProductBtn'), 1, FailureHandling.OPTIONAL)

    while (crossBtnFound) {
        WebUI.click(findTestObject('Basket/basket_DeleteProductBtn'), FailureHandling.CONTINUE_ON_FAILURE)

        crossBtnFound = WebUI.verifyElementPresent(findTestObject('Basket/basket_DeleteProductBtn'), 1, FailureHandling.OPTIONAL)
    }
}

///
'mouse over the my account btn'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

'go to shopping list'
WebUI.click(findTestObject('Home/home_shoppingList'))

//make sure the default shopping list is selected
WebUI.click(findTestObject('ShoppingList/shoppingList_dropdownArrow'))

WebUI.click(findTestObject('ShoppingList/shoppingList_cce'))

'select CCE, 0 = CCE, 1 = CCS and others'
WebUI.selectOptionByValue(findTestObject('ShoppingList/shoppingList_dropdown'), '0', true)

///
'Add list twice'
for (int addToBasketCount = 1; addToBasketCount <= 2; addToBasketCount++) {
    WebUI.click(findTestObject('ShoppingList/shoppingList_AddAllToBasketBtn'))

    WebUI.click(findTestObject('ShoppingList/shoppingList_RemoveAllFromListPopUp_cancelBtn'))
}

'check cart total should be 6'
WebUI.verifyElementText(findTestObject('GlobalObjects/basket_ItemCount'), '2')

'go to basket'
WebUI.click(findTestObject('GlobalObjects/basket_Icon'))

WebUI.focus(findTestObject('Basket/Delivery Method/basket_CCchkbox'))

'select CC'
WebUI.click(findTestObject('Basket/Delivery Method/basket_CCchkbox'))

'checkout'
WebUI.click(findTestObject('Basket/basket_leftCheckoutBtn'))

'redeem points if point redemption test is required'
if (GlobalVariable.pointRedemptionTest) {
    WebUI.mouseOver(findTestObject('Checkout/checkout_eWallet'))

    'open ewallet'
    WebUI.click(findTestObject('Checkout/checkout_eWallet'))

    WebUI.setText(findTestObject('Checkout/checkout_pointRedemptionField'), '1')

    WebUI.click(findTestObject('Checkout/checkout_pointRedemptionbtn'))
}

Boolean viewMapBtnIsNotVisible = WebUI.verifyElementNotVisible(findTestObject('Checkout/CC - Select Store/checkout_selectCCStore_viewMapBtn'), 
    FailureHandling.OPTIONAL)

'select a store if none selected'
if (viewMapBtnIsNotVisible) {
    'click select store btn'
    WebUI.click(findTestObject('Checkout/CC - Select Store/checkout_selectCCStoreBtn'))

    'open the town dropdown'
    WebUI.click(findTestObject('Checkout/CC - Select Store/checkout_selectCCStore_townDropdownArrow'))

    WebUI.click(findTestObject('Checkout/CC - Select Store/checkout_selectCCStore_townDropdownBox'))

    WebUI.selectOptionByValue(findTestObject('Checkout/CC - Select Store/checkout_selectCCStore_townDropdown'), '台北市', 
        false)

    'confirm selection'
    WebUI.click(findTestObject('Checkout/CC - Select Store/checkout_selectCCStore_confirmStoreBtn'))
}

'select payment method'
WebUI.click(findTestObject('Checkout/Payment Method/checkout_CreditCardOneTimePaymentBtn'))

WebUI.click(findTestObject('Checkout/Payment Method/checkout_CathayBankCardRadioBtn'))

'pay'
WebUI.click(findTestObject('Checkout/checkout_PayBtn'))

'enter credit card credentials'
WebUI.setText(findTestObject('PaymentGateway/CathayBank/paymentGateway_CathayBank_cardNoField'), GlobalVariable.cathayBankCreditCardNo)

WebUI.selectOptionByValue(findTestObject('PaymentGateway/CathayBank/paymentGateway_CathayBank_monthField'), GlobalVariable.cathayBankCreditCardMonth, 
    false)

WebUI.selectOptionByValue(findTestObject('PaymentGateway/CathayBank/paymentGateway_CathayBank_yearField'), GlobalVariable.cathayBankCreditCardYear, 
    false)

WebUI.setText(findTestObject('PaymentGateway/CathayBank/paymentGateway_CathayBank_cvvField'), GlobalVariable.cathayBankCreditCardCVV)

'confirm credentials'
WebUI.click(findTestObject('PaymentGateway/CathayBank/paymentGateway_CathayBank_confirmBtn'))

'check thank you page heading visible'
WebUI.verifyTextPresent(GlobalVariable.thankYouPageHeading, false)
	
GlobalVariable.pt_cce = WebUI.getText(findTestObject('ThankYouPage/thankYouPage_orderNumber'))



