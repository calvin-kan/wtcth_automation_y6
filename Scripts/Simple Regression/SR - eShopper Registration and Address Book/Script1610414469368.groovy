import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.concurrent.ThreadLocalRandom as Keyword

'Generate random number'
String randomNumber = String.valueOf(Math.abs(new Random().nextInt() % (99999999 - 11111111)) + 11111111)

'Declare strings'
String eShopperEmail = (('autotesting-' + randomNumber) + GlobalVariable.BU) + '@yopmail.com'

String eShopperFirstNameEnglish = 'Auto Testing'

String eShopperLastNameEnglish = 'ENG eShopper'

String eShopperFirstNameOtherLang = 'Auto Testing ' + randomNumber

String eShopperLastNameOtherLang = 'TH eShopper'

String eShopperMobile = '09' + randomNumber

WebUI.openBrowser('')

'Go to WTCTW UAT'
WebUI.navigateToUrl(GlobalVariable.defaultURL)

'Go to my account'
WebUI.enhancedClick(findTestObject('Home/home_MyAccountBtn'))

//Registration
'click register button'
WebUI.enhancedClick(findTestObject('Login/login_RegisterButton'))

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_english_firstName'), eShopperFirstNameEnglish)

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_english_lastName'), eShopperLastNameEnglish)

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_thai_firstName'), eShopperFirstNameOtherLang)

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_thai_lastName'), eShopperLastNameOtherLang)

'Paste email'
WebUI.sendKeys(findTestObject('Registration/registration_eShopperEmailField'), eShopperEmail)

'Paste email again'
WebUI.sendKeys(findTestObject('Registration/registration_eShopperEmailVerField'), eShopperEmail)

'Enter TW mobile'
WebUI.sendKeys(findTestObject('Registration/registration_eShopperMobileField'), eShopperMobile)

'Enter pw'
WebUI.sendKeys(findTestObject('Registration/registration_eShopperPwField'), GlobalVariable.defaultPassword)

'Enter pw again'
WebUI.sendKeys(findTestObject('Registration/registration_eShopperPwVerField'), GlobalVariable.defaultPassword)

'submit'
WebUI.enhancedClick(findTestObject('Registration/registration_SubmitButton'))

'check registration successful'
WebUI.verifyTextPresent(GlobalVariable.reg_Success_Welcome, false)

WebUI.verifyTextPresent(GlobalVariable.reg_Success_eShopperText, false)

'Print email and pw to log'
println(((('\n\n####\nEmail : ' + eShopperEmail) + '\nPassword : ') + GlobalVariable.defaultPassword) + '\n####')

//Address Book
WebUI.enhancedClick(findTestObject('Home/home_MyAccountBtn'))

'click the address book link'
WebUI.enhancedClick(findTestObject('AccountSummary/account_AddressBook'))

'add address'
WebUI.enhancedClick(findTestObject('AddressBook/addressBook_AddAddress'))

'Enter name with randomly generated number'
WebUI.setText(findTestObject('AddressBook/addressBook_FirstNameField'), eShopperFirstNameEnglish)

WebUI.setText(findTestObject('AddressBook/addressBook_LastNameField'), eShopperLastNameEnglish)

'Enter TH mobile'
WebUI.setText(findTestObject('AddressBook/addressBook_MobileField'), eShopperMobile)

WebUI.setText(findTestObject('AddressBook/addressBook_StreetNumberField'), '43120')

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_ProvinceDropdownBtn'))

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_ProvinceDropdown'))

WebUI.selectOptionByValue(findTestObject('AddressBook/addressBook_Province1'), 'Amnat Charoen', true)

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_DistrictDropdownBtn'))

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_DistrictDropdown'))

WebUI.selectOptionByValue(findTestObject('AddressBook/addressBook_District1'), 'TH_CITY_0124', true)

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_SubDistrictDropdownBtn'))

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_SubDistrictDropdown'))

WebUI.selectOptionByValue(findTestObject('AddressBook/addressBook_SubDistrict1'), 'TH_DISTRICT_0927', true)

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_UpdateAddressBtn'))

'check the name was displayed correctly'
WebUI.verifyTextPresent(eShopperFirstNameEnglish, false)

'check the mobile is displayed correctly'
WebUI.verifyTextPresent(eShopperMobile, false)

'edit address'
WebUI.click(findTestObject('AddressBook/addressBook_EditAddressBtn'))

WebUI.setText(findTestObject('AddressBook/addressBook_StreetNumberField'), '43121')

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_DistrictDropdownBtn'))

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_DistrictDropdown'))

WebUI.selectOptionByValue(findTestObject('AddressBook/addressBook_District1'), 'TH_CITY_0124', true)

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_SubDistrictDropdownBtn'))

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_SubDistrictDropdown'))

WebUI.selectOptionByValue(findTestObject('AddressBook/addressBook_SubDistrict1'), 'TH_DISTRICT_0927', true)

'update address'
WebUI.enhancedClick(findTestObject('AddressBook/addressBook_UpdateAddressBtn'))

WebUI.verifyTextPresent('43121', false)

'delete address'
WebUI.enhancedClick(findTestObject('AddressBook/addressBook_DeleteAddressBtn'))

WebUI.verifyTextNotPresent('43121', false)

