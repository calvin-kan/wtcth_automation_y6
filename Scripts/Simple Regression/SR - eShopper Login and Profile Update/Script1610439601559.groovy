import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Generate random number'
int randomNumber = Math.abs(new Random().nextInt() % 99) + 1

String randomAddress = String.valueOf(randomNumber)

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.defaultURL)

WebUI.enhancedClick(findTestObject('Home/home_MyAccountBtn'))

'fill in login'
WebUI.setText(findTestObject('Login/login_usernameField'), GlobalVariable.defaultLogin)

'fill in pw'
WebUI.setText(findTestObject('Login/login__passwordField'), GlobalVariable.defaultPassword)

'click login btn'
WebUI.enhancedClick(findTestObject('Login/login_loginButton'))

'check if reached ac summary page'
WebUI.verifyElementText(findTestObject('AccountSummary/account_AccountSummaryTitle'), GlobalVariable.acSummary_Heading)

'click update info link'
WebUI.enhancedClick(findTestObject('AccountSummary/account_UpdatePerInfo'), FailureHandling.STOP_ON_FAILURE)

'click edit info btn'
WebUI.enhancedClick(findTestObject('AccountSummary/account_EditPerInfoButton'))

WebUI.setText(findTestObject('AccountSummary/account_confirmEmailField'), GlobalVariable.defaultLogin)

'fill in the fields in the address with random number'
WebUI.setText(findTestObject('AccountSummary/account_HomeAddressNo'), randomAddress)

WebUI.setText(findTestObject('AccountSummary/account_VillageOrBuilding'), randomAddress)

WebUI.setText(findTestObject('AccountSummary/account_Moo'), randomAddress)

WebUI.setText(findTestObject('AccountSummary/account_Soi'), randomAddress)

WebUI.setText(findTestObject('AccountSummary/account_Lane'), randomAddress)

WebUI.setText(findTestObject('AccountSummary/account_SubDistrict'), randomAddress)

WebUI.setText(findTestObject('AccountSummary/account_District'), randomAddress)

'update the address'
WebUI.enhancedClick(findTestObject('AccountSummary/account_ConfirmUpdatePerInfoBtn'))

'check if changes were made correctly'
WebUI.verifyTextPresent(randomAddress, false)

'mouse over the my account btn'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

'click the logout btn'
WebUI.enhancedClick(findTestObject('Home/home_logoutBtn'))

