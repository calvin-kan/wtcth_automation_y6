<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Error Checking</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>fd8e46c4-07cc-4ad5-834c-7ed990c4f929</testSuiteGuid>
   <testCaseLink>
      <guid>65aa0655-4e66-4ddd-b6a3-33a799a83267</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Error Checking/EC - eShopper Registration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4eb906b0-8ff5-45ea-af1d-8ce1ac5fe69f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Error Checking/EC - Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a342942-83c7-4164-8a94-21b1471b088c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Error Checking/EC - Member Registration</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
